function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}


window.onload = function() {
    var username = getCookie("username");
    var log = getCookie("out");
    if(log == "t"){
        deleteAllCookies();
    }
    if(username != "" && username != null){
        document.getElementById("logado").innerHTML = "<li class=\"nav-item dropdown\">"
        +"<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">"
        +"Bem Vindo, "+ username
        +"<span class=\"sr-only\">(current)</span></a>"
        +"<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">"
            +"<a class=\"dropdown-item\" href=\"#\">Minhas Compras</a>"
            +"<a class=\"dropdown-item\" href=\"/perfil/?op=1\">Minha Conta</a>"
            +"<a class=\"dropdown-item\" href=\"/inserir_receitas\">Enviar Receita</a>"
            +"<hr class=\"deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto\" style=\"width: 100%;\">"
            +"<a onclick=\"logout()\" class=\"dropdown-item\" href=\"/\">Sair</p>"
        +"</div>"
        +"</li>";
    }
};


function logout(){
    document.cookie = "out=t; path=/;";
}

function deleteAllCookies() {
    var c = document.cookie.split("; ");
    for (i in c) 
     document.cookie =/^[^=]+/.exec(c[i])[0]+"=;expires=Thu, 01 Jan 1970 00:00:00 GMT"; 
    location.href="/"
}
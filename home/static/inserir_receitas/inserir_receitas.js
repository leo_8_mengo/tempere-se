function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function enviar(){
    var csrftoken = getCookie('csrftoken');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert("Enviado!");
        }
        if (this.status == 400) {	
            document.getElementById('loginbox').innerHTML ="<div class=\"alert alert-danger\" role=\"alert\"\>Usuário ou senha ivalido. </div>";
        }
    };

        var formData = new FormData()
        formData.append("titulo", document.getElementById('titulo').value);
        formData.append("descricao", document.getElementById('descricao').value);
        formData.append("ingredientes", document.getElementById('ingredientes').value);
        formData.append("receita", document.getElementById('modo').value);
        formData.append("dono", getCookie("id"));
        

             
       
        xhttp.open("POST", "/Receitas/", true);
        xhttp.setRequestHeader("X-CSRFToken", csrftoken); 
        xhttp.send(formData);
}
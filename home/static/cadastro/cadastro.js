function display(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById('user-img').innerHTML = "<img class=\"rounded-circle col-sm-2 mb-2 img-fluid\""
                                                            +"src=\""+ e.target.result+"\"" 
                                                            +"alt=\"user-image\">";
    };

        reader.readAsDataURL(input.files[0]);
    }   
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// Faz o cadastro do usuario no banco de dados
function cadastrar(){
    var csrftoken = getCookie('csrftoken');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 201) {
            location.href = "/login";
        }
        if (this.status == 400) {	
            document.getElementById('loginbox').innerHTML ="<div class=\"alert alert-danger\" role=\"alert\"\>Usuário ou senha ivalido. </div>";
        }
    };

        var formData = new FormData()
        formData.append("username", document.getElementById('username').value);
        formData.append("first_name", document.getElementById('nome').value);
        formData.append("last_name", document.getElementById('sobrenome').value);
        formData.append("email", document.getElementById('email').value);
        formData.append("telefone", document.getElementById('telefone').value);
        formData.append("endereco.rua", document.getElementById('rua').value);
        formData.append("endereco.numero", document.getElementById('numero').value);
        formData.append("endereco.bairro", document.getElementById('bairro').value);
        formData.append("endereco.complemento", document.getElementById('complemento').value);
        formData.append("password", document.getElementById('senha').value);

        var input = document.getElementById('file');
        if (input.files && input.files[0]) {
            formData.append("foto", input.files[0], "teije.jpg");       
        }

        
       
        xhttp.open("POST", "/Users/", true);
        xhttp.setRequestHeader("X-CSRFToken", csrftoken); 
        xhttp.send(formData);
}
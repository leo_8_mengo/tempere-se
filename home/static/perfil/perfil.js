function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function alterendereco(id){
    var csrftoken = getCookie('csrftoken');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 201) {
            // location.href = "/login";
        }
        if (this.status == 400) {	
            // document.getElementById('loginbox').innerHTML ="<div class=\"alert alert-danger\" role=\"alert\"\>Usuário ou senha ivalido. </div>";
        }
    };
  
    var formData = new FormData()
    formData.append("rua", document.getElementById('rua').value);
    formData.append("numero", document.getElementById('numero').value);
    formData.append("bairro", document.getElementById('bairro').value);
    formData.append("complemento", document.getElementById('complemento').value);
       
    xhttp.open("PATCH", "/Enderecos/"+id+"/", true);
    xhttp.setRequestHeader("X-CSRFToken", csrftoken); 
    xhttp.send(formData);
}


function alter(id, end){
    var csrftoken = getCookie('csrftoken');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert("Alterações realizadas com sucesso.")
        }
        if (this.status == 400) {	
            // document.getElementById('loginbox').innerHTML ="<div class=\"alert alert-danger\" role=\"alert\"\>Usuário ou senha ivalido. </div>";
        }
    };
  
    var formData = new FormData()
    formData.append("first_name", document.getElementById('nome').value);
    formData.append("last_name", document.getElementById('sobrenome').value);
    formData.append("email", document.getElementById('email').value);
    formData.append("telefone", document.getElementById('telefone').value);
    formData.append("password", "w");
       
    alterendereco(end);

    xhttp.open("PATCH", "/Users/"+id+"/", true);
    xhttp.setRequestHeader("X-CSRFToken", csrftoken); 
    xhttp.send(formData);
}


function sc(id){
    var senha = document.getElementById('senha').value;
    var confirm = document.getElementById('confirm').value;

    if(senha == confirm){
        var csrftoken = getCookie('csrftoken');
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                alert("Alteraçãos realizadas com sucesso.")
            }
            if (this.status == 400) {	
            // document.getElementById('loginbox').innerHTML ="<div class=\"alert alert-danger\" role=\"alert\"\>Usuário ou senha ivalido. </div>";
            }
        };
  
        var formData = new FormData()
        formData.append("password", senha);    
       

        xhttp.open("PATCH", "/Users/"+id+"/", true);
        xhttp.setRequestHeader("X-CSRFToken", csrftoken); 
        xhttp.send(formData);
    }
    else{
        alert("As senhas devem ser iguais");
    }
}
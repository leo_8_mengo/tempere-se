//setup api do facebook e verifica se já está logado
window.fbAsyncInit = function() {
    FB.init({
	    appId      : '507214776457332',
	    xfbml      : true,
	    version    : 'v3.2'
	});
	
	FB.getLoginStatus(function(response) {
		
	  	if (response.status === 'connected') {
	   		// location.href = "../"
	   	} else if (response.status === 'not_authorized') {
			document.getElementById('status').innerHTML = 'We are not logged in.'
	   	} else {  
	   		document.getElementById('status').innerHTML = 'You are not logged into Facebook.';
	   	}
	});
};
//setup api do facebook e verifica se já está logado

// conexão com a api do facebook
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/pt_BR/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// conexão com a api do facebook



// login with facebook with extra permissions
function FBlogin() {
	FB.login(function(response) {
		if (response.authResponse) {
		 console.log('Welcome!  Fetching your information.... ');
		 FB.api('/me', function(response) {
		   console.log('Good to see you, ' + response.name + '.');
		 });
		} else {
		 console.log('User cancelled login or did not fully authorize.');
		}
	});
}
// login with facebook with extra permissions

// getting basic user info
function getInfo() {
	FB.api('/me', 'GET', {fields: 'first_name,last_name,name,id'}, function(response) {
		document.getElementById('status').innerHTML = response.id;
	});
}
// getting basic user info


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}



function getUserInfo(){

}

//Login de usuarios cadastrados pela tempere-se
function login(){
	var csrftoken = getCookie('csrftoken');
	var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			// document.getElementById("demo").innerHTML = this.responseText;
			// this.responseText é a resposta da confirmação de login.  
			var data = JSON.parse(this.responseText)
			document.cookie = "token="+data["token"] +"; path=/;"; 
			document.cookie = "username="+data["username"]+"; path=/;";
			document.cookie = "id="+data["id"]+"; path=/;"; 
			location.href = "/"; 
		}
		if (this.status == 400) {	
			document.getElementById('loginbox').innerHTML ="<div class=\"alert alert-danger\" role=\"alert\"\>Usuário ou senha ivalido. </div>";
		}
  	};
	var formData = new FormData()
	formData.append("username", document.getElementById('username').value);
	formData.append("password", document.getElementById('password').value);

	xhttp.open("POST", "api-token-auth/", true);
	xhttp.setRequestHeader("X-CSRFToken", csrftoken); 
  	xhttp.send(formData);
}
//Login de usuarios cadastrados pela tempere-se


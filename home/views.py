from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login
from Produtos.models import Produtos
from Propagandas.models import Propagandas
from Receitas.models import Receita
from FotoReceitas.models import FotoReceitas
from users.models import User
from Pedidos.models import Pedidos
from Itens.models import Item



def index(request):
    if request.method == 'POST':
        nome = request.POST.get('busca')
        produtos = Produtos.objects.filter(nome__icontains = nome)
    else:
        produtos = Produtos.objects.all()

    propaganda = Propagandas.objects.all()
    context = {
        'produtos': produtos,
        'propaganda': propaganda
        }
    return render(request, 'home/index.html', context)


def receitas(request):
    receitas = Receita.objects.all()
    fto_receitas = FotoReceitas.objects.all()
    context = {
        'receitas': receitas,
        'fto_receitas': fto_receitas
    }  
    return render(request, 'receitas/receitas.html', context)

def produto(request):
    if request.method == 'GET':
        idd = request.GET.get("prod") 
        produto = Produtos.objects.filter(id=idd)
        more = Produtos.objects.all()
        mais = more.random(5)
        context = {
            'produto': produto,
            'mais': mais,
        }    
        return render(request, 'produto/produto.html', context)

def disprecipe(request):
    if request.method == 'GET':
        idd = request.GET.get("rec")
        recipe = Receita.objects.filter(id=idd)
        others = Receita.objects.all()
        outros = others.random(4)
        context = {
            'recipe': recipe,
            'outros': outros,
        }
    return render(request, 'disprecipe/disprecipe.html', context)

def login(request):
    return render(request, 'login/login.html')

def cadastro(request):
    return render(request, 'cadastro/cadastro.html')

def loginsubmit(request):
    if request.method == 'POST':
        login = request.POST.get('user')
        senha = request.POST.get('senha')
        user = authenticate(username=login, password=senha)
        if user is not None:
            # login(request, user)
            return redirect('/login/')
        
def perfil(request):
    if request.method == 'GET':
        op = request.GET.get("op")

        id = request.COOKIES.get('id')
        receita = Receita.objects.filter(dono=id)
        pedidos = Pedidos.objects.filter(dono=id)
        
        user = User.objects.filter(id=id)
        context = {
            'user': user,
            'op': op,
            'receita': receita,
            'pedidos': pedidos,
        }
        return render(request, 'perfil/perfil.html', context)
    
def inserir_receitas(request):
    return render(request, 'inserir_receitas/inserir_receitas.html')
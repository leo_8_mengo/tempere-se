from django.urls import path
from . import views

app_name = 'home'
urlpatterns = [
    path('', views.index, name='index'),
    path('receitas/', views.receitas, name='receitas'),
    path('produto/', views.produto, name='produto'),
    path('login/', views.login, name='login'),
    path('disprecipe/', views.disprecipe, name='display_recipe'),
    path('login/loginsubmit', views.loginsubmit, name='loginsubmit'),
    path('cadastro/', views.cadastro, name='cadastro'),
    path('perfil/', views.perfil, name='perfil'),
    path('inserir_receitas', views.inserir_receitas, name='inserir_receitas')
]
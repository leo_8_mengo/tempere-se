from django.db import models
from django.contrib.auth.models import User
from Enderecos.models import Endereco

class User(User):
    telefone = models.CharField(max_length=150, null=True)
    foto = models.ImageField(upload_to="Perfil", null=True, blank=True)
    endereco = models.ForeignKey(Endereco, on_delete=models.DO_NOTHING, null=True, blank=True)
    
def __str__(self):
    return self.username
    
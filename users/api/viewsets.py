from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from users.models import User
from .serializer import UserSerializer


class UsersViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['POST'], detail=True)
    def create_user(request):
        serialized = UserSerializer(data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)


    
    # def update(self, request, *args, **kwargs):
    #     return super(UsersViewSet, self).update(request, *args, **kwargs)     

   
from rest_framework.serializers import ModelSerializer, CharField
from users.models import User
from Enderecos.models import Endereco
from Enderecos.api.serializer import EnderecosSerializer
from django.contrib.auth import get_user_model

UserModel = get_user_model()

class UserSerializer(ModelSerializer):
    password = CharField(write_only=True)
    endereco = EnderecosSerializer()

    class Meta:
        model = User
        fields = ('id','username','first_name', 'last_name', 'email', 'password', 'endereco','telefone', 'foto')


    def create(self, validated_data):
        endereco = validated_data.pop('endereco')

        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.endereco = EnderecosSerializer.create(endereco)
        

        user.save()
        return user


    def update(self, instance, validated_data):
        password = validated_data.pop('password')
        user = super(UserSerializer, self).update(instance, validated_data)
        if password != "w": 
            user.set_password(password)
        user.save()
        return user     
     
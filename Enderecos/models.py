from django.db import models

class Endereco(models.Model):
    rua = models.CharField(max_length=150)
    bairro = models.CharField(max_length=150)
    numero = models.CharField( max_length=150)
    complemento = models.CharField(max_length=150)
    localizacao = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.rua
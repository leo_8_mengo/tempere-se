from rest_framework.serializers import ModelSerializer
from Enderecos.models import Endereco
from rest_framework.validators import UniqueTogetherValidator

class EnderecosSerializer(ModelSerializer):
    class Meta:
        model = Endereco
        fields = ('id', 'rua', 'bairro', 'numero', 'complemento', 'localizacao')

        # validators = [
        #     UniqueTogetherValidator(
        #         queryset=Endereco.objects.all(),
        #         fields=('rua', 'bairro', 'numero')
        #     )
        # ]

    def create(validated_data):
        if not Endereco.objects.filter(**validated_data):
            endereco = Endereco.objects.create(**validated_data)
            endereco.save()
            return endereco
        else:
            return Endereco.objects.filter(**validated_data)[0]    
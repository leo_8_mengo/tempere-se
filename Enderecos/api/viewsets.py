from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from Enderecos.models import Endereco
from .serializer import EnderecosSerializer
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.authentication import TokenAuthentication

class EnderecosViewSet(ModelViewSet):
    serializer_class = EnderecosSerializer
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = (TokenAuthentication,)

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        rua = self.request.query_params.get('rua', None)
        bairro = self.request.query_params.get('bairro', None)
        queryset = Endereco.objects.all()

        if id:
            queryset = Endereco.objects.filter(pk=id)
        
        if rua:
            queryset = Endereco.objects.filter(rua__iexact=rua)

        if bairro:
            queryset = Endereco.objects.filter(bairro__iexact=bairro)
        

        return queryset
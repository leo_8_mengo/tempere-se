# Generated by Django 2.1.3 on 2019-01-07 20:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Receitas', '0003_auto_20190104_0140'),
    ]

    operations = [
        migrations.AddField(
            model_name='receita',
            name='capa',
            field=models.ImageField(blank=True, null=True, upload_to='Receitas_capa'),
        ),
    ]

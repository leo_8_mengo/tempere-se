from rest_framework.serializers import ModelSerializer
from Receitas.models import Receita

class ReceitasSerializer(ModelSerializer):
    class Meta():
        model = Receita
        fields = ('id','dono', 'titulo', 'receita','data')
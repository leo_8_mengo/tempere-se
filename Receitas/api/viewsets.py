from rest_framework.viewsets import ModelViewSet
from Receitas.models import Receita
from .serializer import ReceitasSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication


class ReceitasViewSet(ModelViewSet):
    serializer_class = ReceitasSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = (TokenAuthentication,)

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        titulo = self.request.query_params.get('titulo', None)
        queryset = Receita.objects.all()

        if id:
            queryset = Receita.objects.filter(pk=id)
        
        if titulo:
            queryset = Receita.objects.filter(titulo__iexact=titulo)     

        return queryset
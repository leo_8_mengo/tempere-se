from django.db import models
# from django.contrib.auth.models import User
from django_random_queryset import RandomManager
from users.models import User

class Receita(models.Model):
    dono = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    capa = models.ImageField(upload_to="Receitas_capa", null=True, blank=True)
    titulo = models.CharField(max_length=120, null=True, blank=True)
    descricao = models.TextField(null=True, blank=True) 
    ingredientes = models.TextField(null=True, blank=True) 
    receita = models.TextField(null=True, blank=True)
    data = models.DateTimeField(auto_now_add=True)
    objects = RandomManager()
    

    def __str__(self):
        return self.titulo
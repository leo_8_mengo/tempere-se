from rest_framework.serializers import ModelSerializer
from Itens.models import Item

class ItemSerializer(ModelSerializer):
    class Meta():
        model = Item
        fields = ('id','produto','pedido','qtde')
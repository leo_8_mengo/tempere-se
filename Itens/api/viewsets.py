from rest_framework.viewsets import ModelViewSet
from Itens.models import Item
from .serializer import ItemSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication


class ItensViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ItemSerializer

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        produto = self.request.query_params.get('produto', None)
        pedido = self.request.query_params.get('pedido', None)
        queryset = Item.objects.all()

        if id:
            queryset = Item.objects.filter(pk=id)
        
        if produto:
            queryset = Item.objects.filter(produto=produto)
        
        if pedido:
            queryset = Item.objects.filter(pedido=pedido)
        return queryset
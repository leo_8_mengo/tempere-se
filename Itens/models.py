from django.db import models
from Produtos.models import Produtos
from Pedidos.models import Pedidos

class Item(models.Model):
    produto = models.ForeignKey(Produtos, on_delete=models.CASCADE)
    pedido = models.ForeignKey(Pedidos, on_delete=models.CASCADE)
    qtde = models.DecimalField(max_digits=3, decimal_places=1)
    

    def __str__(self):
        return self.produto.nome
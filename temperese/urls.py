"""temperese URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static
from Produtos.api.viewsets import ProdutosViewSet
from Pedidos.api.viewsets import PedidosViewSet
from Itens.api.viewsets import ItensViewSet
from Receitas.api.viewsets import ReceitasViewSet
from FotoReceitas.api.viewsets import FotoReceitasViewSet
from Enderecos.api.viewsets import EnderecosViewSet
from users.api.viewsets import UsersViewSet
from Propagandas.api.viewsets import PropagandasViewSet
from rest_framework.authtoken.views import obtain_auth_token


router = routers.DefaultRouter()
router.register(r'Produtos', ProdutosViewSet, base_name="Produtos")
router.register(r'Receitas', ReceitasViewSet, base_name="Receita")
router.register(r'FotoReceitas', FotoReceitasViewSet, base_name="FotoReceitas")
router.register(r'Enderecos', EnderecosViewSet, base_name="Endereco")
router.register(r'Pedidos', PedidosViewSet, base_name="Pedidos")
router.register(r'Itens', ItensViewSet, base_name="Item")
router.register(r'Users', UsersViewSet)
router.register(r'Propagandas', PropagandasViewSet,  base_name="Propagandas")


urlpatterns = [
    url(r'^', include(router.urls)),
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    url('api-token-auth', obtain_auth_token),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



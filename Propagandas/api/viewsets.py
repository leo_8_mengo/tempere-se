from rest_framework.viewsets import ModelViewSet
from Propagandas.models import Propagandas
from .serializer import PropagandasSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication

class PropagandasViewSet(ModelViewSet):
    serializer_class = PropagandasSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = (TokenAuthentication,)

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        queryset = Propagandas.objects.all()

        if id:
            queryset = Propagandas.objects.filter(pk=id)
        
        return queryset
from rest_framework.serializers import ModelSerializer
from Propagandas.models import Propagandas

class PropagandasSerializer(ModelSerializer):
    class Meta:
        model = Propagandas
        fields = ('id','nome', 'publi')
from django.db import models

class Propagandas(models.Model):
    nome = models.CharField(max_length=150, null=True, blank=True)
    publi = models.ImageField(upload_to="Propagandas", null=True, blank=True)

    def __str__(self):
        return self.nome
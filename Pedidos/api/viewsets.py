from rest_framework.viewsets import ModelViewSet
from Pedidos.models import Pedidos
from .serializer import PedidosSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

class PedidosViewSet(ModelViewSet):
    serializer_class = PedidosSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        dono = self.request.query_params.get('dono', None)
        data = self.request.query_params.get('data', None)
        queryset = Pedidos.objects.all()

        if id:
            queryset = Pedidos.objects.filter(pk=id)
        
        if dono:
            queryset = Pedidos.objects.filter(dono=dono)
        
        if data:
            queryset = Pedidos.objects.filter(data__iexact=data)
        return queryset
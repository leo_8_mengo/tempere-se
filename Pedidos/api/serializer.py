from rest_framework.serializers import ModelSerializer
from Pedidos.models import Pedidos
from users.api.serializer import UserSerializer

class PedidosSerializer(ModelSerializer):
    
    class Meta:
        model = Pedidos
        fields = ('id', 'dono', 'data', 'status')        

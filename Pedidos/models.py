from django.db import models
# from django.contrib.auth.models import User
from users.models import User

class Pedidos(models.Model):
    dono = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.DateTimeField(auto_now_add=True)
    status = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.dono.username
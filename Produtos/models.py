from django.db import models
from django_random_queryset import RandomManager

class Produtos(models.Model):
    nome = models.CharField(max_length=150)
    descricao = models.TextField(null=True, blank=True)
    estoque = models.DecimalField(max_digits=4, decimal_places=2)
    foto = models.ImageField(upload_to="Produto", null=True, blank=True)
    preco = models.CharField(max_length=25, null=True, blank=True)
    objects = RandomManager()
    
    def __str__(self):
        return self.nome
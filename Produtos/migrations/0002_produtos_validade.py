# Generated by Django 2.1.3 on 2018-12-05 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Produtos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='produtos',
            name='validade',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]

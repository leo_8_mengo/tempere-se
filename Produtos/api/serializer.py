from rest_framework.serializers import ModelSerializer
from Produtos.models import Produtos

class PodutosSerializer(ModelSerializer):
    class Meta:
        model = Produtos
        fields = ('id', 'nome', 'descricao', 'estoque', 'foto')

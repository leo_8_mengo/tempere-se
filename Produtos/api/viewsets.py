from rest_framework.viewsets import ModelViewSet
from Produtos.models import Produtos
from .serializer import PodutosSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication

class ProdutosViewSet(ModelViewSet):
    serializer_class = PodutosSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = (TokenAuthentication,)

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        nome = self.request.query_params.get('nome', None)
        queryset = Produtos.objects.all()

        if id:
            queryset = Produtos.objects.filter(pk=id)
        
        if nome:
            queryset = Produtos.objects.filter(nome__iexact=nome)
        

        return queryset
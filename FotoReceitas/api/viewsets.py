from rest_framework.viewsets import ModelViewSet
from FotoReceitas.models import FotoReceitas
from .serializer import FotoReceitasSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication


class FotoReceitasViewSet(ModelViewSet):
    serializer_class = FotoReceitasSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    authentication_classes = (TokenAuthentication,)

    def get_queryset(self):
        id = self.request.query_params.get('id', None)
        receita = self.request.query_params.get('receita', None)
        queryset = FotoReceitas.objects.all()

        if id:
            queryset = FotoReceitas.objects.filter(pk=id)
        
        if receita:
            queryset = FotoReceitas.objects.filter(receita=receita)     

        return queryset
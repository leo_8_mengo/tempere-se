from rest_framework.serializers import ModelSerializer
from FotoReceitas.models import FotoReceitas

class FotoReceitasSerializer(ModelSerializer):
    class Meta():
        model = FotoReceitas
        fields = ('id','foto','receita')
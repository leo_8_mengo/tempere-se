from django.db import models
from Receitas.models import Receita

class FotoReceitas(models.Model):
    receita = models.ForeignKey(Receita, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to="Receitas", null=True, blank=True)

def __str__(self):
    return self.receita.titulo    